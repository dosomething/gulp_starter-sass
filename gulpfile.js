// 宣告 gulp 物件
var argv = require('minimist')(process.argv.slice(2)),
     runSequence = require('run-sequence'),
     gulp = require('gulp'),
     gutil = require('gulp-util'),
     watch = require('gulp-watch'),
     jade = require('gulp-jade'),
     compass = require('gulp-compass'),
     yaml = require('gulp-yaml'), // 必須安裝 js-yaml
     jsonFormat = require('gulp-json-format'),
     data = require('gulp-data'),
     cleanCSS = require('gulp-clean-css'),
     concat = require('gulp-concat'),
     sourcemaps = require('gulp-sourcemaps'),
     vendor = require('gulp-concat-vendor'),
     uglify = require('gulp-uglify'),
     imagemin = require('gulp-imagemin'),
     Path = require("path"),
     connect = require('gulp-connect'),
     deploy = require('gulp-gh-pages'),
     vinylftp = require('vinyl-ftp'),
     browserSync = require('browser-sync').create();

var ftppass = require('./json/.ftppass.json'); // ftp 帳密

var opacity = function (css, opts) {
    css.eachDecl(function(decl) {
        if (decl.prop === 'opacity') {
            decl.parent.insertAfter(decl, {
                prop: '-ms-filter',
                value: '"progid:DXImageTransform.Microsoft.Alpha(Opacity=' + (parseFloat(decl.value) * 100) + ')"'
            });
        }
    });
};

// CLI options
var enabled = {
  // Enable static asset revisioning when `--production`
  rev: argv.production,
  // Disable source maps when `--production`
  maps: !argv.production,
  // Fail styles task on error when `--production`
  failStyleTask: argv.production,
  // Fail due to JSHint warnings only when `--production`
  failJSHint: argv.production,
  // Strip debug statments from javascript when `--production`
  stripJSDebug: argv.production
};

dev_path = {
  jade:     './src/jade/',
  js:        './src/js/',
  css:      './src/postcss/',
  sass:     './src/sass/',
  images:  './src/images/'
};

build_path = {
  html:     './dist/',
  css:      './dist/assets/css/',
  js:        './dist/assets/js/',
  images:  './dist/assets/images/',
  json:      './json/'
};

// chrome plugin glup-devtools
module.exports = gulp;

// 顯示錯誤資訊
function swallowError (error) {
  // If you want details of the error in the console
  console.log(error.toString());
  this.emit('end');
}


// // 宣告 normalize 路徑
// var path = {
//   normalize: "bower_components/normalize-css/normalize.css",
// };

// livereload server
gulp.task('connect', function() {
  connect.server({
    root: 'dist',
    port: '8888',
    livereload: true
  });
});



// 讀 yaml 檔轉 json
gulp.task('yaml', function() {
  return gulp.src('./*y{,a}ml')
  .pipe(yaml())
  .on('error', console.log)
  .pipe(gulp.dest(build_path.json));
});

gulp.task('jsonFormat', ['yaml'], function() {
  return gulp.src(build_path.json + '*.json')
    .pipe(jsonFormat(4))
    .pipe(gulp.dest(build_path.json));
});

// 編譯 jade
gulp.task('jade', ['jsonFormat'], function() {
  return gulp.src([dev_path.jade + '*.jade', '!' + dev_path.jade + '/**/_*.jade'])
    .pipe(data((function(file) {
      return require(build_path.json + "site.json");
    })))
    .pipe(data((function(file) {
      return require(build_path.json + "data.json");
    })))
    .pipe(jade({
      pretty: true
    }))
    .on('error', swallowError)
    .pipe(gulp.dest(build_path.html))
    .pipe(connect.reload());
});

// 編譯 css ( sass )
gulp.task('sass', function() {
    return gulp.src([dev_path.sass + '*.scss', '!' + dev_path.sass + '/**/_*.scss'])
       .pipe(compass({
            // sourcemap: true,
            time: true,
            css: build_path.css,
            sass: dev_path.sass,
            style: 'compact' //nested, expanded, compact, compressed
       }))
       .pipe(gulp.dest(build_path.css))
       .pipe(connect.reload());
});

// 壓縮圖片
gulp.task('imagemin', function(){
  gulp.src( dev_path.images + '*.{png,jpg,gif,svg,ico}')
    .pipe(imagemin())
    .pipe(gulp.dest( build_path.images ))
    .pipe(connect.reload());
});

// 編譯 js
gulp.task('js', function(){
  return gulp.src( dev_path.js + '*.js')
    .pipe(uglify()) // uglify js
    .on('error', swallowError)
    .pipe(gulp.dest( build_path.js ))
    .pipe(connect.reload());
});

// 產生 vendor js
// 注意！這邊是有使用 bower 的情況下用
gulp.task('vendor-scripts', function() {
  gulp.src('bower_components/**/**/*.min.js')
  .pipe(vendor('vendor.js'))
  .pipe(gulp.dest( build_path.js ));
});
// 產生 plugin js
gulp.task('plugin-scripts', function() {
  gulp.src('src/plugin/*.js')
  .pipe(vendor('plugin.js'))
  .pipe(gulp.dest( build_path.js ));
});
//browser-sync
gulp.task('bro-sync', ['jade','sass','js'], function() {
  browserSync.init({
      server: build_path.html
  });
  gulp.watch( dev_path.css + "**/*.scss", ['sass']);
  gulp.watch( dev_path.jade + "**/*.jade", ['jade']);
  gulp.watch( dev_path.js + "*.js", ['js']);
});

// 監聽檔案
gulp.task('watch', function(){
  // gulp.watch('src/slim/**/*.slim', ['slim']);
  gulp.watch( dev_path.jade + '**/*.jade', ['jade']);
  gulp.watch( dev_path.sass + '**/*.scss', ['sass']);
  gulp.watch( dev_path.js + '*.js', ['js']);
  gulp.watch( dev_path.images + '*.{png,jpg,gif,svg,ico}', ['imagemin']);
});



// 預設啟動 gulp
// gulp.task('default', ['connect', 'jade', 'sass', 'js', 'imagemin', 'watch']);

// 發佈至 git
gulp.task('deploy-git', function(){
  return gulp.src(['./**/*', '!' + build_path.html + '**/*'])
    .pipe(deploy({
      branch: 'master'
    }));
});

// 發佈至 github pages
gulp.task('deploy-ghpages', function(){
  return gulp.src( build_path.html + '**/*')
    .pipe(deploy());
});

// ### upload
gulp.task('upload', function(callback){
  runSequence('rmdirdist', 'ftpupload', callback);
});

// ### Remove remote dist directory
gulp.task('rmdirdist', function(cb){
  var conn = vinylftp.create(ftppass);
  conn.rmdir( ftppass.path , cb);
});

// ### Upload Vinyl FTP
gulp.task('ftpupload', function(callback){
  var conn = vinylftp.create({
    host:       ftppass.host,
    user:       ftppass.user,
    password: ftppass.password,
    log:        gutil.log
  });
  var globs = [
    build_path.html + '**/*'
  ];
  // using base = '.' will transfer everything to /public_html correctly
  // turn off buffering in gulp.src for best performance
  // return gulp.src( globs, { base: '.', buffer: false } )
  return gulp.src( globs, { buffer: false } )
    .pipe( conn.newer( ftppass.path ) ) // only upload newer files
    .pipe( conn.dest( ftppass.path ) );
});



// ### Build
gulp.task('build', function(callback) {
  var tasks = [
    'jade',
    'sass',
    'js',
    'imagemin'
  ];

  if (argv.production) {
    // only add upload to task list if `--production`
    tasks = tasks.concat(['upload']);
  }

  if (argv.dev) {
    // add connect and watch to task list if `--dev`
    tasks = tasks.concat(['connect', 'watch']);
  }

  if (argv.ghpages) {
    // only add deploy-git to task list if `--ghpages`
    tasks = tasks.concat(['deploy-ghpages']);
  }

  runSequence.apply(
    this,
    tasks.concat([callback])
  );
});
